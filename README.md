# The PHP Practitioner

### Tipos de FetchAll (Alguns)
```php
// Normal
$array->fetchAll();
/// stdClass
$array->fetchAll(PDO::FETCH_OBJ);
// Classe criada
$array->fetchAll(PDO::FETCH_CLASS, 'Classe');
```

### Como chamar uma função estática
```php
Classe::funcao();
```

### Array Filtering
```php
$var = array_map(function ($item) {
    return $item * 2;
}, $array);

$var2 = array_filter($array, function($item) {
    $return $item->key === false;
});

// retorna direto o valor da coluna especificada,
// Se passar um terceiro parametro, esse parametro do array vira a key do retorno
$var3 = array_column($item, 'coluna');
```
