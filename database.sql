CREATE DATABASE mytodo;
USE mytodo;

CREATE TABLE todos (
    id INTEGER PRIMARY KEY AUTO_INCREMENT, 
    description TEXT NOT NULL,
    completed BOOLEAN NOT NULL
);

INSERT INTO todos (description, completed) VALUES 
('Go to the store', false),
('Finish up screencast', true);

CREATE TABLE users(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL
);
