<?php 

class PagesController 
{
    public function home()
    {
        $users = App::get('database')->selectAll('users');

        require 'views/index.view.php';
    }

    public function about()
    {
        require 'views/about.view.php';
    }

    public function contact()
    {
        require 'views/contact.view.php';
    }

    public function callAction($controller, $action)
    {
        if(! method_exists($controller, $action)) {
            throw new Exception(
                "{$controller} does not respond to the {$action} action."
            );
        }

        return (new $controller)->$action();
    }
}